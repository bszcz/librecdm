LibreCDM
========

Stochastic Reaction-Diffusion Simulation of Cyclic Dominance Metapopulation (CDM)


References
----------

Szczesny, Bartosz (2014) Coevolutionary Dynamics in Structured Populations of Three Species, PhD Thesis, University of Leeds, [http://etheses.whiterose.ac.uk/7547](http://etheses.whiterose.ac.uk/7547)


COPYRIGHT / LICENSE
-------------------

    Stochastic Reaction-Diffusion Simulation
    of Cyclic Dominance Metapopulation (CDM)
    Copyright (c) 2015 Bartosz Szczesny

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
