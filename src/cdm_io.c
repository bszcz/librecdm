// Copyright (c) 2015 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cdm.h"

static void cdm_io_file_name(char* file_name, const struct config conf, const double t) {
	if (streq(conf.init_type, "random")) {
		sprintf(
			file_name,
			"%s/cdm_%d_%d__L%d_N%d__b%.2f_s%.2f_z%.2f_m%.2f__d%.2f_e%.2f__t%022.17f_.%s",
			conf.data_path, conf.init_seed, conf.react_seed,
			conf.lattice_size, conf.patch_size,
			conf.beta, conf.sigma, conf.zeta, conf.mu,
			conf.delta_d, conf.delta_e, t, CDM_FILE_EXT
		);
	} else {
		sprintf(
			file_name,
			"%s/cdm__%d__L%d_N%d__b%.2f_s%.2f_z%.2f_m%.2f__d%.2f_e%.2f__t%022.17f_.%s",
			conf.data_path, conf.react_seed,
			conf.lattice_size, conf.patch_size,
			conf.beta, conf.sigma, conf.zeta, conf.mu,
			conf.delta_d, conf.delta_e, t, CDM_FILE_EXT
		);
	}
}

int cdm_io_save(const struct datum* dat, const struct config conf, const double t) {
	char file_name[CDM_STR_LEN];
	cdm_io_file_name(file_name, conf, t);
	printf("cdm_io_save: %s\n", file_name);
	return datum_write_gz(dat, file_name);
}

struct datum* cdm_io_load(const struct config conf, const double t) {
	char file_name[CDM_STR_LEN];
	cdm_io_file_name(file_name, conf, t);
	printf("cdm_io_load: %s\n", file_name);
	return datum_read_gz(file_name);
}
