// Copyright (c) 2015 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cdm.h"

void cdm_image_rgb(struct pixmap* img, struct datum* dat, struct config conf) {
	for (int k = 0; k < (3 * img->width * img->height); k++) {
		double fraction = dat->data[k] / (double)conf.patch_size;
		img->bytes[k] = (255 * fraction) + 0.5;
	}
}

void cdm_image(
	void (*cdm_image_func)(struct pixmap* img, struct datum* dat, struct config conf),
	struct pixmap* img, struct datum* dat, struct config conf,
	char* file_name_head, char* file_name_tail
) {
	(*cdm_image_func)(img, dat, conf);
	char file_name[CDM_STR_LEN];
	snprintf(file_name, CDM_STR_LEN, "%s%s", file_name_head, file_name_tail);
	pixmap_write_png(img, file_name);
	printf("cdm_image: %s\n", file_name);
}

int main(int argc, char** argv) {
	struct config conf = cdm_config_parse(argc, argv);
	cdm_config_print(conf);
	const int ff = cdm_config_first_file(argc, argv);
	if (ff == -1) {
		exit(EXIT_FAILURE);
	}

	for (int i = ff; i < argc; i++) {
		printf("\n");
		printf("cdm_image: %s\n", argv[i]);

		char file_name_head[CDM_STR_LEN];
		strncpy(file_name_head, argv[i], CDM_STR_LEN);
		const int ext_pos = strlen(file_name_head) - strlen(CDM_FILE_EXT) - 1; // -1 for '.'
		file_name_head[ext_pos] = '\0'; // remove extension with '.' before it

		struct datum* dat = datum_read_gz(argv[i]);
		if (dat == NULL) {
			exit(EXIT_FAILURE);
		}
		struct pixmap* img = pixmap_alloc(dat->lens[0], dat->lens[1]);
		if (img == NULL) {
			datum_free(dat);
			exit(EXIT_FAILURE);
		}

		if (conf.image_rgb) {
			cdm_image(cdm_image_rgb, img, dat, conf, file_name_head, "rgb.png");
		}

		pixmap_free(img);
		datum_free(dat);
	}

	exit(EXIT_SUCCESS);
}
