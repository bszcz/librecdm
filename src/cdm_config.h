// Copyright (c) 2015 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#pragma once

#include "cdm.h"

struct config {
	char	data_path[CDM_STR_LEN];

	int	react_seed;
	int	init_seed;
	char	init_type[CDM_STR_LEN];

	double	beta;
	double	sigma;
	double	zeta;
	double	mu;
	double	delta_d;
	double	delta_e;

	// rescaled rates
	double	beta_nn1;
	double	sigma_nn1;
	double	zeta_nn1;
	double	mu_n;
	double	delta_d_nn;
	double	delta_e_nn;

	int	lattice_size;
	int	patch_size;
	int	num_reactions;
	int	num_species;

	double	start_time;
	double	save_dt;
	int	num_saves;

	int	image_rgb;
};

struct config cdm_config_parse(const int argc, char** argv);

int cdm_config_first_file(const int argc, char** argv);

void cdm_config_print(const struct config conf);
