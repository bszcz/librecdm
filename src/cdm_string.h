// Copyright (c) 2015 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#pragma once

#include "cdm.h"

int streq(const char* str1, const char* str2);

char* strdiv(char* str1, const char del);

char* strtrim(char* str);
