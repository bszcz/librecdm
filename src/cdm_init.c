// Copyright (c) 2015 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cdm.h"

void cdm_init_random(struct datum* dat, const int init_seed, const int num_species, const int patch_size) {
	srand(init_seed);
	for (int i = 0; i < dat->lens[0]; i++) {
		for (int j = 0; j < dat->lens[1]; j++) {
			for (int n = 0; n < patch_size; n++) {
				int r = rand() % (num_species + 1);
				if (r == num_species) {
					continue; // empty space
				}
				int k = datum_index(dat, i, j, r);
				dat->data[k] += 1;
			}
		}
	}
}

void cdm_init_ordered(struct datum* dat, const int patch_size) {
	for (int i = 0; i < dat->lens[0]; i++) {
		for (int j = 0; j < dat->lens[1]; j++) {
			int species = 0;
			if (i > (dat->lens[0] / 2)) {
				if (j < (dat->lens[1] / 2)) {
					species = 1;
				} else {
					species = 2;
				}
			}
			int k = datum_index(dat, i, j, species);
			dat->data[k] = patch_size;
		}
	}
}

int main(int argc, char** argv) {
	struct config conf = cdm_config_parse(argc, argv);
	cdm_config_print(conf);

	const int ndims = 3;
	const int lens[] = { conf.lattice_size, conf.lattice_size, conf.num_species };
	struct datum* dat = datum_alloc(ndims, lens);
	if (dat == NULL) {
		exit(EXIT_FAILURE);
	}

	if (streq("random", conf.init_type)) {
		cdm_init_random(dat, conf.init_seed, conf.num_species, conf.patch_size);
	} else if (streq("ordered", conf.init_type)) {
		cdm_init_ordered(dat, conf.patch_size);
	} else {
		printf("cdm_init: invalid initialisation type '%s'\n", conf.init_type);
	}

	const double start_time = 0.0;
	cdm_io_save(dat, conf, start_time);
	datum_free(dat);
	exit(EXIT_SUCCESS);
}
