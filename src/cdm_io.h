// Copyright (c) 2015 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#pragma once

#include "cdm.h"

int cdm_io_save(const struct datum* dat, const struct config conf, const double t);

struct datum* cdm_io_load(const struct config conf, const double t);
