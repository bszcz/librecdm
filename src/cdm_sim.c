// Copyright (c) 2015 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cdm.h"

struct simulation* cdm_sim_alloc(struct config conf) {
	struct simulation* sim = malloc(sizeof(struct simulation));
	if (sim == NULL) {
		printf("cdm_sim_alloc: cannot allocate memory\n");
		return NULL;
	}

	const int num_sites = conf.lattice_size * conf.lattice_size;
	const int num_nodes = (2 * num_sites) - 1; // binary tree size
	sim->lattice    = malloc(num_sites * conf.num_species   * sizeof(int));
	sim->props      = malloc(num_sites * conf.num_reactions * sizeof(double));
	sim->props_tree = malloc(num_nodes * sizeof(double));
	if (
		sim->lattice    == NULL ||
		sim->props      == NULL ||
		sim->props_tree == NULL
	) {
		printf("cdm_sim_alloc: cannot allocate memory\n");
		cdm_sim_free(sim);
		return NULL;
	}
	sim->props_leaves = sim->props_tree + (num_sites - 1); // pointer shortcut

	sim->conf = conf;
	sim->t = conf.start_time;

	return sim;
}

void cdm_sim_free(struct simulation* sim) {
	if (sim != NULL) {
		free(sim->lattice);
		free(sim->props);
		free(sim->props_tree);
		sim->lattice = NULL;
		sim->props = NULL;
		sim->props_tree = NULL;
		sim->props_leaves = NULL;
	}
}

inline
int cdm_sim_parent_index(const int node_index) {
	return (node_index - 1) / 2;
}

inline
int cdm_sim_left_child_index(const int node_index) {
	return (2 * node_index) + 1;
}

inline
int cdm_sim_right_child_index(const int node_index) {
	return (2 * node_index) + 2;
}

inline
int cdm_sim_leaf_index(struct simulation* sim, const int i, const int j) {
	const int lattice_size = sim->conf.lattice_size;
	const int ii = (i + lattice_size) % lattice_size;
	const int jj = (j + lattice_size) % lattice_size;
	return (ii * lattice_size) + jj;
}

inline
int cdm_sim_tree_index(struct simulation* sim, const int i, const int j) {
	const int first_leaf_index = (sim->conf.lattice_size * sim->conf.lattice_size) - 1;
	return first_leaf_index + cdm_sim_leaf_index(sim, i, j);
}

inline
int cdm_sim_prop_index(struct simulation* sim, const int i, const int j, const int r) {
	const int lattice_size = sim->conf.lattice_size;
	const int ir = ((i + lattice_size) % lattice_size) * sim->conf.num_reactions;
	const int jr = ((j + lattice_size) % lattice_size) * sim->conf.num_reactions;
	return (ir * lattice_size) + jr + r;
}
#define T(i, j, r) sim->props[cdm_sim_prop_index(sim, i, j, r)]

inline
int cdm_sim_lattice_index(struct simulation* sim, const int i, const int j, const int s) {
	const int lattice_size = sim->conf.lattice_size;
	const int is = ((i + lattice_size) % lattice_size) * sim->conf.num_species;
	const int js = ((j + lattice_size) % lattice_size) * sim->conf.num_species;
	return (is * lattice_size) + js + s;
}
#define A(i, j) sim->lattice[cdm_sim_lattice_index(sim, i, j, 0)]
#define B(i, j) sim->lattice[cdm_sim_lattice_index(sim, i, j, 1)]
#define C(i, j) sim->lattice[cdm_sim_lattice_index(sim, i, j, 2)]
#define E(i, j) (sim->conf.patch_size - A(i, j) - B(i, j) - C(i, j))

void cdm_sim_calc_prop(struct simulation* sim, const int i, const int j) {
	double sum = 0.0;
	// birth
	sum += T(i, j,  0) = sim->conf.beta_nn1  * A(i, j) * E(i, j);
	sum += T(i, j,  1) = sim->conf.beta_nn1  * B(i, j) * E(i, j);
	sum += T(i, j,  2) = sim->conf.beta_nn1  * C(i, j) * E(i, j);
	// selection
	sum += T(i, j,  3) = sim->conf.sigma_nn1 * C(i, j) * A(i, j);
	sum += T(i, j,  4) = sim->conf.sigma_nn1 * A(i, j) * B(i, j);
	sum += T(i, j,  5) = sim->conf.sigma_nn1 * B(i, j) * C(i, j);
	// zero-sum
	sum += T(i, j,  6) = sim->conf.zeta_nn1  * C(i, j) * A(i, j);
	sum += T(i, j,  7) = sim->conf.zeta_nn1  * A(i, j) * B(i, j);
	sum += T(i, j,  8) = sim->conf.zeta_nn1  * B(i, j) * C(i, j);
	// mutation
	sum += T(i, j,  9) = sim->conf.mu_n * A(i, j);
	sum += T(i, j, 10) = sim->conf.mu_n * A(i, j);
	sum += T(i, j, 11) = sim->conf.mu_n * B(i, j);
	sum += T(i, j, 12) = sim->conf.mu_n * B(i, j);
	sum += T(i, j, 13) = sim->conf.mu_n * C(i, j);
	sum += T(i, j, 14) = sim->conf.mu_n * C(i, j);
	// diffusion in i direction
	sum += T(i, j, 15) = sim->conf.delta_d_nn * E(i, j) * A(i + 1, j);
	sum += T(i, j, 16) = sim->conf.delta_d_nn * E(i, j) * B(i + 1, j);
	sum += T(i, j, 17) = sim->conf.delta_d_nn * E(i, j) * C(i + 1, j);
	sum += T(i, j, 18) = sim->conf.delta_d_nn * A(i, j) * E(i + 1, j);
	sum += T(i, j, 19) = sim->conf.delta_d_nn * B(i, j) * E(i + 1, j);
	sum += T(i, j, 20) = sim->conf.delta_d_nn * C(i, j) * E(i + 1, j);
	// exchanges in i direction
	sum += T(i, j, 21) = sim->conf.delta_e_nn * A(i, j) * B(i + 1, j);
	sum += T(i, j, 22) = sim->conf.delta_e_nn * A(i, j) * C(i + 1, j);
	sum += T(i, j, 23) = sim->conf.delta_e_nn * B(i, j) * C(i + 1, j);
	sum += T(i, j, 24) = sim->conf.delta_e_nn * B(i, j) * A(i + 1, j);
	sum += T(i, j, 25) = sim->conf.delta_e_nn * C(i, j) * A(i + 1, j);
	sum += T(i, j, 26) = sim->conf.delta_e_nn * C(i, j) * B(i + 1, j);
	// diffusion in j direction
	sum += T(i, j, 27) = sim->conf.delta_d_nn * E(i, j) * A(i, j + 1);
	sum += T(i, j, 28) = sim->conf.delta_d_nn * E(i, j) * B(i, j + 1);
	sum += T(i, j, 29) = sim->conf.delta_d_nn * E(i, j) * C(i, j + 1);
	sum += T(i, j, 30) = sim->conf.delta_d_nn * A(i, j) * E(i, j + 1);
	sum += T(i, j, 31) = sim->conf.delta_d_nn * B(i, j) * E(i, j + 1);
	sum += T(i, j, 32) = sim->conf.delta_d_nn * C(i, j) * E(i, j + 1);
	// exchanges in j direction
	sum += T(i, j, 33) = sim->conf.delta_e_nn * A(i, j) * B(i, j + 1);
	sum += T(i, j, 34) = sim->conf.delta_e_nn * A(i, j) * C(i, j + 1);
	sum += T(i, j, 35) = sim->conf.delta_e_nn * B(i, j) * C(i, j + 1);
	sum += T(i, j, 36) = sim->conf.delta_e_nn * B(i, j) * A(i, j + 1);
	sum += T(i, j, 37) = sim->conf.delta_e_nn * C(i, j) * A(i, j + 1);
	sum += T(i, j, 38) = sim->conf.delta_e_nn * C(i, j) * B(i, j + 1);

	sim->props_leaves[cdm_sim_leaf_index(sim, i, j)] = sum;
}

void cdm_sim_update_tree(struct simulation* sim, const int node_index) {
	const int parent_node_index = cdm_sim_parent_index(node_index);
	const int left_child_index = cdm_sim_left_child_index(parent_node_index);
	const int right_child_index = cdm_sim_right_child_index(parent_node_index);

	sim->props_tree[parent_node_index] = sim->props_tree[left_child_index]
		                 + sim->props_tree[right_child_index];

	if (parent_node_index != CDM_ROOT_NODE_INDEX) {
		cdm_sim_update_tree(sim, parent_node_index);
	}
}

void cdm_sim_init_props(struct simulation* sim) {
	for (int i = 0; i < sim->conf.lattice_size; i++) {
		for (int j = 0; j < sim->conf.lattice_size; j++) {
			cdm_sim_calc_prop(sim, i, j);
			cdm_sim_update_tree(sim, cdm_sim_tree_index(sim, i, j));
		}
	}
}

void cdm_sim_react_search(struct simulation* sim, double prop_rand, int node_index) {
	const int lattice_size = sim->conf.lattice_size;
	const int first_leaf_index = (lattice_size * lattice_size) - 1;
	if (node_index < first_leaf_index) {
		const int left_child_index = cdm_sim_left_child_index(node_index);
		const double left_child_prop = sim->props_tree[left_child_index];
		if (prop_rand <= left_child_prop) {
			cdm_sim_react_search(sim, prop_rand, left_child_index);
		} else {
			prop_rand -= left_child_prop; // trim to search right child node
			const int right_child_index = cdm_sim_right_child_index(node_index);
			cdm_sim_react_search(sim, prop_rand, right_child_index);
		}
	} else {
		sim->next_i = (node_index - first_leaf_index) / lattice_size;
		sim->next_j = (node_index - first_leaf_index) % lattice_size;

		if (prop_rand >= sim->props_tree[node_index]) {
			// search backwards if prop_rand exceeds sum of
			// propensities at (i,j) due to rounding errors
			for (int r = sim->conf.num_reactions; r >= 0; r--) {
				if (T(sim->next_i, sim->next_j, r) > 0) {
					sim->next_r = r;
					return;
				}
			}
		} else {
			for (int r = 0; r < sim->conf.num_reactions; r++) {
				prop_rand -= T(sim->next_i, sim->next_j, r);
				if (prop_rand <= 0.0) {
					sim->next_r = r;
					return;
				}
			}
		}
	}
}

void cdm_sim_react_execute(struct simulation* sim) {
	const int i = sim->next_i;
	const int j = sim->next_j;

	switch (sim->next_r) {
	// birth
	case  0: A(i, j)++; break;
	case  1: B(i, j)++; break;
	case  2: C(i, j)++; break;
	// selection
	case  3: A(i, j)--; break;
	case  4: B(i, j)--; break;
	case  5: C(i, j)--; break;
	// zero-sum
	case  6: A(i, j)--; C(i, j)++; break;
	case  7: B(i, j)--; A(i, j)++; break;
	case  8: C(i, j)--; B(i, j)++; break;
	// mutation
	case  9: A(i, j)--; B(i, j)++; break;
	case 10: A(i, j)--; C(i, j)++; break;
	case 11: B(i, j)--; C(i, j)++; break;
	case 12: B(i, j)--; A(i, j)++; break;
	case 13: C(i, j)--; A(i, j)++; break;
	case 14: C(i, j)--; B(i, j)++; break;
	// diffusion in i direction
	case 15: A(i, j)++; A(i + 1, j)--; break;
	case 16: B(i, j)++; B(i + 1, j)--; break;
	case 17: C(i, j)++; C(i + 1, j)--; break;
	case 18: A(i, j)--; A(i + 1, j)++; break;
	case 19: B(i, j)--; B(i + 1, j)++; break;
	case 20: C(i, j)--; C(i + 1, j)++; break;
	// exchanges in i direction
	case 21: A(i, j)--; A(i + 1, j)++; B(i, j)++; B(i + 1, j)--; break;
	case 22: A(i, j)--; A(i + 1, j)++; C(i, j)++; C(i + 1, j)--; break;
	case 23: B(i, j)--; B(i + 1, j)++; C(i, j)++; C(i + 1, j)--; break;
	case 24: B(i, j)--; B(i + 1, j)++; A(i, j)++; A(i + 1, j)--; break;
	case 25: C(i, j)--; C(i + 1, j)++; A(i, j)++; A(i + 1, j)--; break;
	case 26: C(i, j)--; C(i + 1, j)++; B(i, j)++; B(i + 1, j)--; break;
	// diffusion in j direction
	case 27: A(i, j)++; A(i, j + 1)--; break;
	case 28: B(i, j)++; B(i, j + 1)--; break;
	case 29: C(i, j)++; C(i, j + 1)--; break;
	case 30: A(i, j)--; A(i, j + 1)++; break;
	case 31: B(i, j)--; B(i, j + 1)++; break;
	case 32: C(i, j)--; C(i, j + 1)++; break;
	// exchanges in j direction
	case 33: A(i, j)--; A(i, j + 1)++; B(i, j)++; B(i, j + 1)--; break;
	case 34: A(i, j)--; A(i, j + 1)++; C(i, j)++; C(i, j + 1)--; break;
	case 35: B(i, j)--; B(i, j + 1)++; C(i, j)++; C(i, j + 1)--; break;
	case 36: B(i, j)--; B(i, j + 1)++; A(i, j)++; A(i, j + 1)--; break;
	case 37: C(i, j)--; C(i, j + 1)++; A(i, j)++; A(i, j + 1)--; break;
	case 38: C(i, j)--; C(i, j + 1)++; B(i, j)++; B(i, j + 1)--; break;

	default: printf("cdm_sim_react_execute: error: reaction number = %d\n", sim->next_r);
	}
}

void cdm_sim_update_props(struct simulation* sim) {
	/*
	x - reaction event at (i, j)
	c - changed  due to reaction
	a - affected due to reaction
	. - unaffected and unchanged

	.a.
	ax.  - reaction inside the patch, sim->next_r in (0..14)
	...

	.aa
	axc  - migration in i direction, sim->next_r in (15..26)
	...

	.a.
	ax.  - migration in j direction, sim->next_r in (27..38)
	ac.
	*/
	const int i = sim->next_i;
	const int j = sim->next_j;
	const int r = sim->next_r;

	cdm_sim_calc_prop(sim, i    , j    );
	cdm_sim_calc_prop(sim, i - 1, j    );
	cdm_sim_calc_prop(sim, i    , j - 1);
	if (r > 14) { // migration reaction
		if (r < 27) { // in i direction
			cdm_sim_calc_prop(sim, i + 1, j - 1);
			cdm_sim_calc_prop(sim, i + 1, j    );
		} else {      // in j direction
			cdm_sim_calc_prop(sim, i - 1, j + 1);
			cdm_sim_calc_prop(sim, i    , j + 1);
		}
	}

	cdm_sim_update_tree(sim, cdm_sim_tree_index(sim, i    , j    ));
	cdm_sim_update_tree(sim, cdm_sim_tree_index(sim, i - 1, j    ));
	cdm_sim_update_tree(sim, cdm_sim_tree_index(sim, i    , j - 1));
	if (r > 14) { // migration reaction
		if (r < 27) { // in i direction
			cdm_sim_update_tree(sim, cdm_sim_tree_index(sim, i + 1, j - 1));
			cdm_sim_update_tree(sim, cdm_sim_tree_index(sim, i + 1, j    ));
		} else {      // in j direction
			cdm_sim_update_tree(sim, cdm_sim_tree_index(sim, i - 1, j + 1));
			cdm_sim_update_tree(sim, cdm_sim_tree_index(sim, i    , j + 1));
		}
	}
}

double cdm_sim_react(struct simulation* sim) {
	const double total_prop = sim->props_tree[CDM_ROOT_NODE_INDEX];
	const double non_zero_uni_rand = 1.0 - ((double)rand() / (double)RAND_MAX); // (0, 1]
	const double dt = (-1.0) * log(non_zero_uni_rand) / total_prop / sim->conf.patch_size;

	const double prop_rand = total_prop * ((double)rand() / (double)RAND_MAX);
	cdm_sim_react_search(sim, prop_rand, CDM_ROOT_NODE_INDEX);
	cdm_sim_react_execute(sim);
	cdm_sim_update_props(sim);

	return dt;
}
