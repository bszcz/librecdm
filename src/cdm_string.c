// Copyright (c) 2015 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cdm.h"

int streq(const char* str1, const char* str2) {
	return 0 == strcmp(str1, str2);
}

char* strdiv(char* str1, const char del) {
	char* str2 = NULL;
	while (*str1) {
		if (*str1 == del) {
			*str1 = '\0';
			str2 = str1 + 1;
			break;
		}
		str1++;
	}
	return str2;
}

char* strtrim(char* str) {
	if (str == NULL) {
		return NULL;
	}
	char* c = str;
	while (isspace((int)(*c))) {
		*c = '\0';
		c++;
	}
	if (*c == '\0') {
		return NULL;
	}
	str = c; // new beginning
	c += strlen(str) - 1;
	while (isspace((int)(*c))) {
		*c = '\0';
		c--;
	}
	return str;
}
