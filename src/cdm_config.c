// Copyright (c) 2015 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cdm.h"

void cdm_config_print(const struct config conf) {
	printf("cdm_config_print:\n");

	printf("\tdata_path = %s\n", conf.data_path);

	printf("\treact_seed = %d\n", conf.react_seed);
	printf("\tinit_seed  = %d\n", conf.init_seed);
	printf("\tinit_type  = %s\n", conf.init_type);

	printf("\tbeta  = %f\n", conf.beta);
	printf("\tsigma = %f\n", conf.sigma);
	printf("\tzeta  = %f\n", conf.zeta);
	printf("\tmu    = %f\n", conf.mu);
	printf("\tdelta_d = %f\n", conf.delta_d);
	printf("\tdelta_e = %f\n", conf.delta_e);

	printf("\tlattice_size = %d\n", conf.lattice_size);
	printf("\tpatch_size   = %d\n", conf.patch_size);

	printf("\tstart_time = %f\n", conf.start_time);
	printf("\tsave_dt    = %f\n", conf.save_dt);
	printf("\tnum_saves  = %d\n", conf.num_saves);
}

static void cdm_config_set_opt(struct config* confp, char* opt, char* val) {
	if (streq(opt, "data_path")) {
		strncpy(confp->data_path, val, CDM_STR_LEN);
		return;
	}

	if (streq(opt, "react_seed")) {
		confp->react_seed = strtol(val, NULL, 10);
		return;
	}
	if (streq(opt, "init_seed")) {
		confp->init_seed = strtol(val, NULL, 10);
		return;
	}
	if (streq(opt, "init_type")) {
		strncpy(confp->init_type, val, CDM_STR_LEN);
		return;
	}

	if (streq(opt, "beta")) {
		confp->beta = strtod(val, NULL);
		return;
	}
	if (streq(opt, "sigma")) {
		confp->sigma = strtod(val, NULL);
		return;
	}
	if (streq(opt, "zeta")) {
		confp->zeta = strtod(val, NULL);
		return;
	}
	if (streq(opt, "mu")) {
		confp->mu = strtod(val, NULL);
		return;
	}
	if (streq(opt, "delta_d")) {
		confp->delta_d = strtod(val, NULL);
		return;
	}
	if (streq(opt, "delta_e")) {
		confp->delta_e = strtod(val, NULL);
		return;
	}

	if (streq(opt, "lattice_size")) {
		confp->lattice_size = strtol(val, NULL, 10);
		return;
	}
	if (streq(opt, "patch_size")) {
		confp->patch_size = strtol(val, NULL, 10);
		return;
	}

	if (streq(opt, "start_time")) {
		confp->start_time = strtod(val, NULL);
		return;
	}
	if (streq(opt, "save_dt")) {
		confp->save_dt = strtod(val, NULL);
		return;
	}
	if (streq(opt, "num_saves")) {
		confp->num_saves = strtod(val, NULL);
		return;
	}

	if (streq(opt, "image_rgb")) {
		confp->image_rgb = strtol(val, NULL, 10);
		return;
	}

	printf("cdm_config_set_opt: invalid option '%s'\n", opt);
	exit(EXIT_FAILURE);
}

void cdm_config_calc_rates(struct config* confp) {
	const double n = confp->patch_size;
	confp->beta_nn1  = confp->beta  / n / (n - 1.0);
	confp->sigma_nn1 = confp->sigma / n / (n - 1.0);
	confp->zeta_nn1  = confp->zeta  / n / (n - 1.0);
	confp->mu_n      = confp->mu    / n;
	confp->delta_d_nn = confp->delta_d_nn / n / n;
	confp->delta_e_nn = confp->delta_e_nn / n / n;
}

void cdm_config_usage(char* prog_name) {
	printf("cdm_config_usage: %s "
	       "--config <config_file> "
	       "opt1=val1 opt2=val2 ... "
	       "--files <data_file_1> <data_file_2> ...\n",
	       prog_name);
}

int cdm_config_first_file(const int argc, char** argv) {
	for (int i = 0; i < argc - 1; i++) { // argc - 1 since a flag at the end is useless
		if (streq("-f", argv[i]) || streq("--files", argv[i])) {
			return i + 1;
		}
	}
	printf("cdm_config_first_file: no files given\n");
	cdm_config_usage(argv[0]);
	return -1; // files missing if not found in the loop
}

static void cdm_config_parse_pair(struct config* confp, char* pair) {
	strdiv(pair, '#'); // remove comment
	char* val = strtrim(strdiv(pair, '='));
	char* opt = strtrim(pair);
	if (opt && val) {
		cdm_config_set_opt(confp, opt, val);
	} else if ((! opt) && (! val)) {
		// empty line in config file
	} else {
		printf("cdm_config_parse_pair: invalid option ('%s' = '%s')\n", opt, val);
		exit(EXIT_FAILURE);
	}
}

static void cdm_config_parse_file(struct config* confp, char* filename) {
	FILE* fp = fopen(filename, "rb");
	if (fp == NULL) {
		printf("cdm_config_parse_file: cannot open config file '%s'\n", filename);
		exit(EXIT_FAILURE);
	}
	char pair[CDM_STR_LEN] = { 0 };
	while (NULL != fgets(pair, CDM_STR_LEN, fp)) {
		cdm_config_parse_pair(confp, pair);
	}
	fclose(fp);
}

struct config cdm_config_parse(const int argc, char** argv) {
	if (argc == 1) {
		printf("cdm_config_parse: no arguments given\n");
		cdm_config_usage(argv[0]);
		exit(EXIT_FAILURE);
	}

	struct config conf;
	for (int i = 1; i < argc; i++) {
		if (streq("-f", argv[i]) || streq("--files", argv[i])) {
			break;
		}
		if (streq("-c", argv[i]) || streq("--config", argv[i])) {
			if (argc > i + 1) {
				cdm_config_parse_file(&conf, argv[i + 1]);
				i++;
			} else {
				printf("cdm_config_parse: config file name missing\n");
			}
		} else if (streq("-h", argv[i]) || streq("--help", argv[i])) {
			cdm_config_usage(argv[0]);
			exit(EXIT_SUCCESS);
		} else {
			cdm_config_parse_pair(&conf, argv[i]);
		}
	}

	if (conf.init_seed == 0) {
		srand(time(NULL));
		conf.init_seed = rand();
	}
	conf.num_species = 3;
	conf.num_reactions = 39;

	cdm_config_calc_rates(&conf);

	return conf;
}
