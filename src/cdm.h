// Copyright (c) 2015 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#pragma once

#include <ctype.h>
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "datum/datum_gz.h"
#include "pixmap/pixmap_png.h"

#define CDM_STR_LEN 512

#include "cdm_config.h"
#include "cdm_io.h"
#include "cdm_sim.h"
#include "cdm_string.h"

static const double CDM_PI = 3.14159265358979323846;
static const char CDM_FILE_EXT[] = "datum.gz";
