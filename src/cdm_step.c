// Copyright (c) 2015 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#include "cdm.h"

int cdm_step_load(struct simulation* sim);
int cdm_step_save(struct simulation* sim);

int main(int argc, char** argv) {
	struct config conf = cdm_config_parse(argc, argv);
	cdm_config_print(conf);
	struct simulation* sim = cdm_sim_alloc(conf);
	if (sim == NULL) {
		exit(EXIT_FAILURE);
	}

	int err = cdm_step_load(sim);
	if (err != 0) {
		cdm_sim_free(sim);
		exit(EXIT_FAILURE);
	}

	cdm_sim_init_props(sim);

	srand(sim->conf.react_seed);
	sim->t = cdm_sim_react(sim);

	err = cdm_step_save(sim);
	if (err != 0) {
		cdm_sim_free(sim);
		exit(EXIT_FAILURE);
	}

	cdm_sim_free(sim);
	exit(EXIT_SUCCESS);
}

int cdm_step_load(struct simulation* sim) {
	struct datum* dat = cdm_io_load(sim->conf, sim->t);
	if (dat == NULL) {
		return 1;
	}
	for (int k = 0; k < dat->len; k++) {
		sim->lattice[k] = (int)dat->data[k];
	}
	datum_free(dat);
	return 0;
}

int cdm_step_save(struct simulation* sim) {
	const int ndims = 3;
	const int lens[] = { sim->conf.lattice_size, sim->conf.lattice_size, sim->conf.num_species };
	struct datum* dat = datum_alloc(ndims, lens);
	if (dat == NULL) {
		return 1;
	}
	for (int k = 0; k < dat->len; k++) {
		dat->data[k] = (double)sim->lattice[k];
	}
	cdm_io_save(dat, sim->conf, sim->t);
	datum_free(dat);
	return 0;
}
