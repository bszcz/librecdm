// Copyright (c) 2015 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the GPLv2 license.

#pragma once

static const int CDM_ROOT_NODE_INDEX = 0;

struct simulation {
	int*    lattice;
	int     next_i; // next reaction - x coordinate
	int     next_j; // next reaction - y coordinate
	int     next_r; // next reaction - reaction num.
	double  t;            // current simulation time
	double* props;        // transition propensities
	double* props_tree;   // summed in a binary tree
	double* props_leaves; // shortcut to tree leaves

	struct config conf;
};

struct simulation* cdm_sim_alloc(struct config conf);

void cdm_sim_free(struct simulation* sim);

void cdm_sim_init_props(struct simulation* sim);

double cdm_sim_react(struct simulation* sim);
