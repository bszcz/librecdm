FLAGS=-g -std=c99 -Wall -Wextra -O3 -I/usr/lib/gcc/x86_64-linux-gnu/4.4/include -I/usr/lib/gcc/x86_64-linux-gnu/4.6/include -I/usr/include
VPATH=src:src/datum:src/pixmap

all: cdm_init cdm_image cdm_step

datum_deps=obj/datum.o obj/datum_gz.o -lz
pixmap_deps=obj/pixmap.o obj/pixmap_png.o -lpng

cdm_common_deps=obj/cdm_config.o obj/cdm_string.o obj/cdm_io.o $(datum_deps)

cdm_init_deps=src/cdm_init.c $(cdm_common_deps)
cdm_init: $(cdm_init_deps)
	$(CC) $(FLAGS) -o bin/cdm_init $(cdm_init_deps)

cdm_image_deps=src/cdm_image.c $(cdm_common_deps) $(pixmap_deps)
cdm_image: $(cdm_image_deps)
	$(CC) $(FLAGS) -o bin/cdm_image $(cdm_image_deps)

cdm_step_deps=src/cdm_step.c $(cdm_common_deps) $(pixmap_deps) obj/cdm_sim.o -lm
cdm_step: $(cdm_step_deps)
	$(CC) $(FLAGS) -o bin/cdm_step $(cdm_step_deps)

obj/%.o: %.c %.h
	$(CC) $(FLAGS) -c $< -o obj/$(@F)

clean:
	rm -fv bin/* obj/*.o
